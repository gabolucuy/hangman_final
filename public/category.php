<!DOCTYPE html>
<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "hangman"; 
$id=0;
$conn = mysqli_connect($servername, $username, $password, $database);
if (!$conn)
{
	die("Fallo en la conexión con la Base de Datos. ):" 
	   . mysqli_connect_error());
}
	$success = false;
if (isset($_POST["newWordButton"]))
{
	$wordName = $_POST["newWordTextBox"];
	$sql = "INSERT INTO Words(word) VALUES ('" . $wordName . "')";
	
	if (mysqli_query($conn, $sql))
	{
		$lastId = mysqli_insert_id($conn);
		
	}
}
$selectSql = "SELECT id, word FROM Words";
$result = mysqli_query($conn, $selectSql);
?>
<html lang="es-BO">
  <head>
    <title>El ahorcado - Categoría: Películas</title>
	<meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/admin.css">
  </head>
  <body>
    <h1>Palabras para Categoría "Películas"</h1>
	<ul>
	
		<?php
       if (mysqli_num_rows($result) ) 
	   {
		   while( $row = mysqli_fetch_assoc($result) )
		   { ?>
			  <li><?= $row["word"] ?> <a href="deleteWord.php?id=1">[X]</a></li>  
	 <?php
           }
	   }
	  ?>
	
	
	  <li>
		<form method="POST" action="category.php">
		  <input type="hidden" name="categoryHidden" value="1" />
		  <input type="text" name="newWordTextBox" maxlength="50" autofocus/>
		  <input type="submit" name="newWordButton" value="Crear" />
		</form>
	  </li>
	</ul>
	
	<div>
	   <a href="deleteCategory.php?categoryId=1" onclick="return confirm('¿Está seguro que desea eliminar esta categoría?');">Eliminar esta categoría</a>
	</div>
  </body>
</html>