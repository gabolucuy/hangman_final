-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2016 at 06:14 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hangman`
--

-- --------------------------------------------------------

--
-- Table structure for table `animales`
--

CREATE TABLE `animales` (
  `Id` int(30) NOT NULL,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Peliculas'),
(2, 'Animales'),
(3, 'Cantantes'),
(4, 'Personajes'),
(5, 'Escritores'),
(6, 'Libros'),
(8, 'Canciones'),
(9, 'Actores');

-- --------------------------------------------------------

--
-- Table structure for table `peliculas`
--

CREATE TABLE `peliculas` (
  `Id` int(30) NOT NULL,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `Id` int(250) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Apellido` varchar(50) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `NumeroTarjeta` int(50) NOT NULL,
  `Contrasena` varchar(50) NOT NULL,
  `Wins` int(50) NOT NULL,
  `Losses` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`Id`, `Nombre`, `Apellido`, `Correo`, `NumeroTarjeta`, `Contrasena`, `Wins`, `Losses`) VALUES
(1, 'Gabriel', 'Lucuy', 'gabolucuy@hotmail.com', 2147483647, '9a8ed596716a71f95e1ee9a330741be18e4ddd0f', 5, 5),
(2, 'Alejandro', 'Perez', 'aleperez@hotmail.com', 2147483647, '5b4a9e08555d97c5e8bc2fb4f3a89ddde501f7c8', 0, 0),
(3, 'Pedro', 'Torrez', 'pedro@hotmail.com', 2147483647, '4410d99cefe57ec2c2cdbd3f1d5cf862bb4fb6f8', 0, 0),
(4, 'Esteban', 'Ramirez', 'esteban@hotmail.com', 2147483647, '431ef4c5da9a59cbbe78df77a53fcc737a3b78fa', 0, 0),
(5, 'Alejandra', 'Siles', 'alejandra@gmail.com', 789654123, 'e2a4e1f68c36f556711eb0627de4fa5575124734', 0, 0),
(6, 'nicole', 'barreto', 'nnnn@hi', 2147483647, '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 0, 0),
(47, 'Juan', 'Claros', 'juan@hotmail.com', 123, 'b49a5780a99ea81284fc0746a78f84a30e4d5c73', 0, 0),
(49, 'Monica', 'Pardo', 'moni@hotmail.com', 2147483647, 'ba92e71c91937ce09b9e642dcdb571606981becc', 0, 0),
(54, 'Teresa', 'Lucuy', 'tere@hotmail.cmo', 2147483647, 'c5374bc4e204c0a7b0aaff3ea66b1fcb1dcc97ab', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `word`
--

CREATE TABLE `word` (
  `id` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `text` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `word`
--

INSERT INTO `word` (`id`, `categoryId`, `text`) VALUES
(1, 1, 'Batman'),
(2, 3, 'Iron Man'),
(3, 3, 'Star Wars'),
(4, 3, 'dsfsad'),
(5, 1, 'sfdasdfsd'),
(6, 2, 'Ballena'),
(7, 2, 'Gato'),
(8, 2, 'perro'),
(9, 1, 'ppp'),
(10, 2, 'sljfds'),
(11, 1, 'fjh'),
(12, 1, 'gkhkj'),
(13, 1, 'dsjfl'),
(14, 1, 'qwewqe');

-- --------------------------------------------------------

--
-- Table structure for table `words`
--

CREATE TABLE `words` (
  `Id` int(255) NOT NULL,
  `word` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `words`
--

INSERT INTO `words` (`Id`, `word`) VALUES
(0, 'Behind the walls'),
(1, 'Spotlight'),
(2, 'Star Wars'),
(0, 'Iron Man'),
(0, 'Toy Story'),
(0, 'Harry Potter');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animales`
--
ALTER TABLE `animales`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `word`
--
ALTER TABLE `word`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `word`
--
ALTER TABLE `word`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
